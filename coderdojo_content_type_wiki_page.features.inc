<?php
/**
 * @file
 * coderdojo_content_type_wiki_page.features.inc
 */

/**
 * Implements hook_node_info().
 */
function coderdojo_content_type_wiki_page_node_info() {
  $items = array(
    'wiki' => array(
      'name' => t('Wiki page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
